import React from 'react';
import Home from './Home';
import Page1 from './Page1';
import Page1x from './Page1x';
import Page2 from './Page2';
import Default from './Default';
import { BrowserRouter as Router, Route, Switch, Link } from 'react-router-dom';

const App = () => {
    return (
        <Router>
            <ul>
                <li><Link to="/">Home</Link></li>
                <li><Link to="/page1">Page 1</Link></li>
                <li><Link to="/page1/x">Page 1x</Link></li>
                <li><Link to="/page2/dadang/nh">Page 2</Link></li>
            </ul>

            <Switch>
                <Route exact path="/"><Home /></Route>
                <Route exact path="/page1"><Page1 /></Route>
                <Route path="/page1/x"><Page1x /></Route>
                <Route path="/page2/:data/:param"><Page2 /></Route>
                <Route path="*">
                    <Default />
                </Route>
            </Switch>
        </Router>
    );
}

export default App;
