import React from 'react';
import { useParams } from "react-router-dom";

const Page2 = () => {
    const {data,param} = useParams();
    return (
        <div>
            Home Page 2: { data } { param }
        </div>
    );
}

export default Page2;
